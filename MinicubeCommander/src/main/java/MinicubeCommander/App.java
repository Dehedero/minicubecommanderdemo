package MinicubeCommander;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.openapi.apis.AppsV1Api;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.*;
import io.kubernetes.client.util.Yaml;

import java.io.File;
import java.io.IOException;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws ApiException, IOException {
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath("http://192.168.0.15:32003");
        Configuration.setDefaultApiClient(apiClient);

        AppsV1Api aApi = new AppsV1Api();
        CoreV1Api cApi = new CoreV1Api();

        //Создание окружения из заглушек
        File mockdepFile = new File("kubeapptwo/depmock.yaml");
        V1Deployment mockdeployment = (V1Deployment) Yaml.load(mockdepFile);
        aApi.createNamespacedDeployment("default", mockdeployment, null, null, null);
        File mocksvcFile = new File("kubeapptwo/servicemock.yaml");
        V1Service mockService = (V1Service) Yaml.load(mocksvcFile);
        cApi.createNamespacedService("default", mockService, null, null, null);

        //Подготовка основного сервиса для тестирования
        File mockconfmap = new File("kubeappone/apponeconfmock.yaml");
        V1ConfigMap configMap = (V1ConfigMap) Yaml.load(mockconfmap);
        cApi.createNamespacedConfigMap("default", configMap, null, null, null);
        File testdeployment = new File("deptest.yaml");
        V1Deployment deployment = (V1Deployment) Yaml.load(testdeployment);
        aApi.createNamespacedDeployment("default", deployment, null, null, null);

        //условный прогон каких-то тестов

        //Сворачивание развернутого
        V1Status result = aApi.deleteNamespacedDeployment(mockdeployment.getMetadata().getName(), "default", null, null, null, null, null, new V1DeleteOptions());
        System.out.println(result);
        result = aApi.deleteNamespacedDeployment(deployment.getMetadata().getName(), "default", null, null, null, null, null, new V1DeleteOptions());
        System.out.println(result);
        result = cApi.deleteNamespacedService(mockService.getMetadata().getName(), "default", null, null, null, null, null, new V1DeleteOptions());
        System.out.println(result);
        result = cApi.deleteNamespacedConfigMap(configMap.getMetadata().getName(), "default", null, null, null, null, null, new V1DeleteOptions());
        System.out.println(result);

    }
}
