package DemoAppTwo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

@RestController
@RequestMapping("/get")
public class MainController {

    @GetMapping
    public String myMethod() throws IOException {
        return "some data";

    }
}
