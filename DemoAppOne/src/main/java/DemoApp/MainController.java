package DemoApp;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

@RestController
@RequestMapping("/some_method")
public class MainController {

    @GetMapping
    public String myMethod() throws IOException {
        String svcName = System.getenv("SERVICE_SUPPLIER");
        if (svcName == null) {
            return "Can't connet to the service " + svcName;
        }
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(svcName, String.class);
        return "requesting to " + svcName + "\nresponce: " + response.getBody();
    }
}
